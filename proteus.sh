#!/bin/bash
#https://bit.ly/2CswldI
apt -y update;
apt -y upgrade;

#Essential Software
apt -y install gmusicbrowser;
apt -y install krita;
apt -y install putty;
apt -y install xtightvncviewer;
apt -y install krita;
apt -y install geany;
apt -y install mpv;
apt -y install tlp;
apt -y install tlp-rdw;
apt -y install tp-smapi-dkms;
apt -y install acpi-call-dkms;
apt -y -t stretch-backports install screenfetch;
apt -y install git;
apt -y install apt-transport-https;
apt -y install curl;
apt -y install openjdk-8-jdk;
apt -y install arc-theme;

#Install Google Chrome Stable
#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -;
#sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list';

#Install Brave-Dev
curl -s https://brave-browser-apt-dev.s3.brave.com/brave-core-nightly.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-dev.gpg add -;
echo "deb [arch=amd64] https://brave-browser-apt-dev.s3.brave.com/ trusty main" | tee /etc/apt/sources.list.d/brave-browser-dev-trusty.list;
echo kernel.unprivileged_userns_clone = 1 | sudo tee /etc/sysctl.d/00-local-userns.conf;

#Installing Papirus Theme
su - proteus -c 'wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/install.sh | DESTDIR="$HOME/.icons" sh';

#Install resilio sync
wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | apt-key add -;
echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | tee /etc/apt/sources.list.d/resilio-sync.list;

#Set screenfetch on boot
echo "if [ -f /usr/bin/screenfetch ]; then screenfetch; fi" >> /home/proteus/.bashrc;

#Installing Discord
cd /home/proteus/Downloads;
wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb";
dpkg -i /home/proteus/Downloads/discord.deb;
rm /home/proteus/Downloads/discord.deb;
apt -y -f install;

#Installing openbox menu file
cd /home/proteus/Downloads;
su - proteus -c 'wget -O /home/proteus/.config/openbox/menu.xml "https://gitlab.com/Proteus4/debian-install-scripts-3/raw/master/proteus-menu.xml"';

#Insatlling openbox keybinds
cd /home/proteus/Downloads;
su - proteus -c 'wget -O /home/proteus/.config/openbox/rc.xml "https://gitlab.com/Proteus4/debian-install-scripts-3/raw/master/rc.xml"';

#Installing NPM
apt -y install curl software-properties-common;
curl -sL https://deb.nodesource.com/setup_10.x |  bash -;
apt -y install nodejs;

#Installing conky config
su - proteus -c 'wget -O /home/proteus/.config/conky/lite.conkyrc "https://gitlab.com/Proteus4/debian-install-scripts-3/raw/master/lite.conkyrc"';

#Set DPI
cd /home/proteus/Downloads;
rm /home/proteus/.Xresources
su - proteus -c 'wget -O /home/proteus/.Xresources "https://gitlab.com/Proteus4/debian-install-scripts-3/raw/master/.Xresources"';

#Installing Windows 10 Dark Theme
#cd /home/proteus/Downloads
#su - proteus -c 'wget -O "/home/proteus/Downloads/Windows 10 Dark" "https://gitlab.com/Proteus4/debian-install-scripts-3/raw/master/Windows_10_Dark.zip?inline=false"';
#unzip "Windows 10 Dark" -d /home/proteus/.themes;
#rm "Windows 10 Dark";
#su - proteus -c 'wget -O /home/proteus/Downloads/master.zip "https://codeload.github.com/B00merang-Artwork/Windows-10/zip/master"';
#unzip master.zip;
#mv Windows-10-master /home/proteus/.icons;
#rm master.zip;
#rm -r Windows-10-master;


#Installing Visual Studio Code
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | apt-key add -;
#add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main";

#Installing new repo software
apt -y update;
#apt -y install google-chrome-stable;
apt -y install brave-browser-dev;
apt -y install resilio-sync;
apt -y install code;
apt -y autoremove;
rm /etc/apt/sources.list.d/google.list
sync;
echo "Please reboot now"

#Installing Better Discord
curl "https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl" > /usr/local/bin/betterdiscordctl;
chown proteus:proteus /usr/local/bin/betterdiscordctl;
chmod +x /usr/local/bin/betterdiscordctl;
su - proteus -c 'nohup discord >& /dev/null &';
until [  -d /home/proteus/.config/discord/0.0.8/modules/discord_desktop_core ]; do
		sleep 1
	done
su - proteus -c 'betterdiscordctl install -s /usr/share';
