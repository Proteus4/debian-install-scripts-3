#!/bin/bash
#https://bit.ly/2YKS5ch
sudo apt -y update;
sudo apt -y upgrade;

#Extra Software
sudo apt -y install gmusicbrowser;
sudo apt -y install krita;
sudo apt -y install putty;
sudo apt -y install xtightvncviewer;
sudo apt -y install krita;
sudo apt -y install tlp;
sudo apt -y install tlp-rdw;
sudo apt -y install tp-smapi-dkms;
sudo apt -y install acpi-call-dkms;
sudo apt -y install screenfetch;
sudo apt -y install conky;

#Install Papirus Icon Theme
sudo add-apt-repository ppa:papirus/papirus -y;

#Install resilio sync
wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | apt-key add -;
echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | tee /etc/apt/sources.list.d/resilio-sync.list;

#Install Brave
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -;
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ trusty main" | sudo tee /etc/apt/sources.list.d/brave-browser-release-trusty.list;

#Set screenfetch on boot
echo "if [ -f /usr/bin/screenfetch ]; then screenfetch; fi" >> /home/proteus/.bashrc;

#Installing Discord
cd /home/proteus/Downloads;
wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb";
dpkg -i /home/proteus/Downloads/discord.deb;
rm /home/proteus/Downloads/discord.deb;
apt -y -f install;

#Installing Visual Studio Code
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -;
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main";

#Set Linux to use local time to not clash with Windows time
timedatectl set-local-rtc 1 --adjust-system-clock;

#Installing conky config
wget -O /home/proteus/.config/conky/lite.conkyrc "https://gitlab.com/Proteus4/debian-install-scripts-3/raw/master/lite.conkyrc";

#Installing new repo software
sudo apt -y update;
sudo apt -y brave-keyring
sudo apt -y install brave-browser;
sudo apt -y install resilio-sync;
sudo apt -y install code;
sudo apt -y autoremove;
sudo apt install papirus-icon-theme -y;

#Installing Better Discord
curl "https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl" > /usr/local/bin/betterdiscordctl;
chmod +x /usr/local/bin/betterdiscordctl;
nohup discord >& /dev/null &;
until [  -d /home/proteus/.config/discord/0.0.8/modules/discord_desktop_core ]; do
		sleep 1
	done
betterdiscordctl install -s /usr/share;